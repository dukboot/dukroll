# dukroll
A CLI tool to help you bundle your ECMAScript into dukboot functions.

It uses [Rollup.js](https://rollupjs.org/) with a set of preconfigured plugins to support:

- using `node_modules` dependencies
- using ES2015 module `import` statements (recommended)
- using Node-style `require()` to require modules (for compatibility)
- transpiling code to the ES5 that the `duktape` engine supports

Dukroll will treat your application ES file as if it is an ES6 module with no exports
during bundling, and then wraps it in a function much like Node does. Except
unlike Node, which passes 5 pseudo-globals (`module`, `exports`, `require`, `__filename`, `__dirname`)
and does its magic wrapping at run time, dukboot just passes one and does it at
build time. Dukboot function files are very close in style to immediately invoked function expressions (IIFEs), so `dukroll` uses the 'iife' format in Rollup and trims off the "immediately invoked" bits so all that's left is a clean function definition.

Unlike Node, which forces you to require each core module individually, all
the dukboot core modules are exposed in one module called `dukboot`. However,
you probably want to use the named export form (seen in the example below) for convenience and clarity.

## Installation

```
npm install https://gitlab.com/dukboot/dukroll --global
```

## Example usage

```js
// main.js
// This example uses a module (from npm) that is in the ./node_modules folder
import pull from 'pull-stream'
import { thread, stdio } from 'dukboot'

if (thread.id() === 0) {
  var c = pull.count()
  c(null, stdio.print)
  c(null, stdio.print)
  c(null, stdio.print)
  c(null, stdio.print)
}
```

```
$ dukroll main.js
```

The bundled result gets written the filename `main.dukboot.js`. Now we can run it with dukboot!

```
$ dukboot main.dukboot.js
null 0
null 1
null 2
null 3
```

## License

Copyright 2017 William Hilton.
Licensed under [The Unlicense](http://unlicense.org/).
