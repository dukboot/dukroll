const rollup = require('rollup').rollup
const babel = require('rollup-plugin-babel')
const commonjs = require('rollup-plugin-commonjs')
const nodeResolve = require('rollup-plugin-node-resolve')
const builtins = require('rollup-plugin-node-builtins')
const json = require('rollup-plugin-json')
const path = require('path')

const file = process.argv[2]

if (typeof file === 'undefined') {
  console.log('Usage: dukroll <main.js>')
  process.exit(0)
}

rollup({
  entry: file,
  external: ['dukboot'],
  plugins: [
    json(),
    babel({
      "presets": [
        [
          "es2015",
          {
            "modules": false
          }
        ]
      ],
      "plugins": [
        "external-helpers"
      ]
    }),
    nodeResolve(),
    commonjs(),
    builtins(),
    {
      name: 'dukroll',
      transformBundle (source, format) {
        return source.replace(/^\(|var dukrollApp = \(/, "").replace(/\(dukboot\)\)\;\n*$/m, '')
      },
    }
  ]
})
.then(bundle => {
  let result = bundle.write({
    format: 'iife',
    // Using CommonJS require commands at the top-level of a module
    // seems to generate a "default" export for the module automatically
    // even though the top-level app has no exports. When that happens
    // the IIFE bundler throws an error if we have not provided a moduleName
    moduleName: 'dukrollApp',
    // exports: 'none',
    globals: {
      // This would shadow a global 'dukboot' with the function argument dukboot.
      // But a malicious user could still figure out how to access the global
      // scope via a shim, like var global = new Function('return this;')();
      // and then obtain a reference global.dukboot.
      // That is why we trim off the II part and compile and execute it as a function
      // rather than assign dukboot to a global and run the original IIFE as a script.
      dukboot: 'dukboot'
    },
    dest: path.join(path.dirname(file), path.basename(file, path.extname(file)) + '.dukboot.js')
  })
})
